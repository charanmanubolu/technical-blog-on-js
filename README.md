# Self Review
## Topics:

### Loops :

### for
  - for loop  Executes a block of code, with a specific number of times, controlled by a counter.
  ```
  for (initialization; condition; iteration) {
  // code block to be executed
}
  ```

### forEach
  - forEach loop Executes a provided function once for each element of an array, and it does not return anything.
  ```
  array.forEach(function(item, index) {
  // code block to be executed
});
  ```

### for .. in
   - for .. in loop is used for to iterate over element by element in the object.
   ```
   for (let key in object) {
  // code block to be executed
}
   ```

### for .. of
  - for ..of loop is used for to iterate over element by element in the array.
  ```
  for(let value of array){
    // code block to be executed
  }
  ```
### while
  - while loop code executes a function as long as the condition is true.
  ```
while(condition){
    //code block to be executed
}
  ```
### Mutable and Immutable Methods (in strings and arrays)

**Mutable Array Methods:**

- **push()**: Adds one or more elements to the end of an array and returns the new length.
- **pop()**: Removes the last element from an array and returns that element.
- **shift()**: Removes the first element from an array and returns that element.
- **unshift()**: Adds one or more elements to the beginning of an array and returns the new length.
- **splice()**: Adds or removes elements from an array and returns the removed elements.
- **reverse()**: Reverses the order of the elements in an array.
- **sort()**: Sorts the elements of an array in place.

**String**
```
let str = "Hello, World!";

// Immutable string methods

console.log(str.slice(0, 5)); // "Hello"
console.log(str.replace("World", "JavaScript")); // "Hello JavaScript!"
console.log(str.toUpperCase()); // "HELLO, WORLD!"

// Strings are immutable, so the original string remains unchanged
console.log(str); // "Hello, World!"
```
**Array**
```
let arr = [1, 2, 3, 4, 5];

// Mutable array methods

arr.push(6); // [1, 2, 3, 4, 5, 6]
arr.pop(); // [1, 2, 3, 4, 5]
arr.shift(); // [2, 3, 4, 5]
arr.unshift(0); // [0, 2, 3, 4, 5]
arr.splice(2, 1, 10); // [0, 2, 10, 4, 5]
arr.reverse(); // [5, 4, 10, 2, 0]
arr.sort(); // [0, 2, 4, 5, 10]

```

**Immutable Array Methods:**

- **slice()**: Returns a shallow copy of a portion of an array into a new array object.
- **concat()**: Returns a new array that is the combination of two or more arrays.
- **map()**: Creates a new array with the results of calling a provided function on every element in the calling array.
- **filter()**: Creates a new array with all elements that pass the test implemented by the provided function.
- **reduce()**: Applies a function against an accumulator and each element in the array to reduce it to a single value.
```
let arr = [1, 2, 3, 4, 5];

// Immutable array methods

let newArr = arr.slice(1, 4); // [2, 4, 5]
console.log(newArr);

let combinedArr = arr.concat([20, 30]); // [0, 2, 4, 5, 10, 20, 30]
console.log(combinedArr);

let mappedArr = arr.map(num => num * 2); // [0, 4, 8, 10, 20]
console.log(mappedArr);

let filteredArr = arr.filter(num => num > 2); // [4, 5, 10]
console.log(filteredArr);

let sum = arr.reduce((acc, num) => acc + num, 0); // 31
console.log(sum);
```
### Pass by Reference and Pass by Value :
  ### Pass by value
  - when a primitive value is passed to the fucntion ,it is passed by value. This is means it is a copy of the value is created and passed to the function .Changes  made inside the function but does not effect the original value.
  ```
  let x = 10;

function increment(num) {
  num++;
  console.log(`Inside function: ${num}`); // 11
}

increment(x);
console.log(`Outside function: ${x}`); // 10
```
### Pass by reference 
- When an object (including arrays) is passed to a function, it is passed by reference. This means that a reference to the object's memory address is passed to the function. Changes made to the object inside the function will affect the original object outside the function.

```
let obj = { name: 'John' };

function changeName(person) {
  person.name = 'Jane';
  console.log(`Inside function: ${person.name}`); // 'Jane'
}

changeName(obj);
console.log(`Outside function: ${obj.name}`); // 'Jane'
```

### Array methods - 
**Basics:**

**Array.pop (mutable)**: Removes the last element from and array and returns it.

  ```
   let fruits = ["apple", "banana", "orange"];
   let lastFruit = fruits.pop(); // ["apple", "banana"]
   console.log(lastFruit); // "orange"   
  ```

**Array.push() (mutable)**: Adds one or more elements to the end of an array and returns the new length.

```
let fruits = ["apple", "banana"];
let length = fruits.push("orange"); // ["apple", "banana", "orange"]
console.log(length); // 3
```

**Array.concat() (immutable)**: Merges two or more arrays and returns a new array.
```
let fruits1 = ["apple", "banana"];
let fruits2 = ["orange", "grape"];
let mergedFruits = fruits1.concat(fruits2); // ["apple", "banana", "orange", "grape"]
console.log(mergedFruits);
```
**Array.slice() (immutable)**: Returns a shallow copy of a portion of an array into a new array object.
```
let fruits = ["apple", "banana", "orange", "grape"];
let slicedFruits = fruits.slice(1, 3); // ["banana", "orange"]
console.log(slicedFruits);
```

**Array.splice() (mutable)**: Changes the contents of an array by removing or replacing existing elements and/or adding new elements in place.
```
let fruits = ["apple", "banana", "orange", "grape"];
let removedFruits = fruits.splice(1, 2, "kiwi", "pear"); // ["apple", "kiwi", "pear", "grape"]
console.log(removedFruits); // ["banana", "orange"]
```
**Array.join() (immutable)**: Joins all elements of an array into a string.
```
let fruits = ["apple", "banana", "orange"];
let fruitsString = fruits.join(", "); // "apple, banana, orange"
console.log(fruitsString);
```

**Array.flat() (immutable)**: Creates a new array with all sub-array elements concatenated into it recursively up to the specified depth.
```
let numbers = [1, 2, [3, 4, [5, 6]]];
let flatNumbers = numbers.flat(2); // [1, 2, 3, 4, 5, 6]
console.log(flatNumbers);
```
**Finding :**

**Array.find() (immutable)**: Returns the value of the first element in the array that satisfies the provided testing function.
```
let numbers = [1, 2, 3, 4, 5];
let firstEvenNumber = numbers.find(num => num % 2 === 0); // 2
console.log(firstEvenNumber);
```

**Array.indexOf() (immutable):** Returns the first index at which a given element can be found in the array, or -1 if it is not present.

```
let fruits = ["apple", "banana", "orange", "banana"];
let index = fruits.indexOf("banana"); // 1
console.log(index);
```

**Array.includes() (immutable)**: Determines whether an array includes a certain value among its entries, returning true or false.
```
let fruits = ["apple", "banana", "orange"];
let hasBanana = fruits.includes("banana"); // true
console.log(hasBanana);
```

**Array.findIndex() (immutable)**: Returns the index of the first element in the array that satisfies the provided testing function. Otherwise, it returns -1.

```
let numbers = [1, 2, 3, 4, 5];
let firstEvenIndex = numbers.findIndex(num => num % 2 === 0); // 1
console.log(firstEvenIndex);
```
**Higher Order Functions:**

**Array.forEach() (immutable)**: Executes a provided function once for each array element.

```let numbers = [1, 2, 3, 4, 5];
numbers.forEach(num => console.log(num * 2));// 2, 4, 6, 8, 10
```
**Array.filter() (immutable):** Creates a new array with all elements that pass the test implemented by the provided function.

```let numbers = [1, 2, 3, 4, 5];
let evenNumbers = numbers.filter(num => num % 2 === 0); // [2, 4]
console.log(evenNumbers);
```

**Array.map() (immutable):** Creates a new array with the results of calling a provided function on every element in the calling array.

```
let numbers = [1, 2, 3, 4, 5];
let doubledNumbers = numbers.map(num => num * 2); // [2, 4, 6, 8, 10]
console.log(doubledNumbers);
```

**Array.reduce() (immutable):** Applies a function against an accumulator and each element in the array to reduce it to a single value.

```
let numbers = [1, 2, 3, 4, 5];
let sum = numbers.reduce((acc, num) => acc + num, 0); // 15
console.log(sum);
```

**Array.sort() (mutable):** Sorts the elements of an array in place and returns the sorted array.

```
let numbers = [4, 2, 5, 1, 3];
numbers.sort((a, b) => a - b); // [1, 2, 3, 4, 5]
console.log(numbers);
```

**Advanced:**

**Array methods chaining:** You can chain multiple array methods together to perform complex operations on an array.

```
let numbers = [1, 2, 3, 4, 5];
let evenDoubledNumbers = numbers
  .filter(num => num % 2 === 0) // [2, 4]
  .map(num => num * 2) // [4, 8]
  .sort((a, b) => b - a); // [8, 4]
console.log(evenDoubledNumbers);
``` 
### String methods 



**str.startsWith() (immutable)**: Determines whether a string begins with the characters of a specified string.

```
let str = "Hello, World!";
console.log(str.startsWith("Hello")); // true
console.log(str.startsWith("World")); // false
```
**str.endsWith() (immutable)**: Determines whether a string ends with the characters of a specified string.
```
let str = "Hello, World!";
console.log(str.endsWith("World!")); // true
console.log(str.endsWith("Hello")); // false

```
**str.charAt() (immutable):** Returns the character at the specified index position.
```
let str = "Hello, World!";
console.log(str.charAt(0)); // "H"
console.log(str.charAt(7)); // "W"
```
**str.repeat() (immutable):** Returns a new string with a specified number of copies of the original string.
```
let str = "Hello ";
console.log(str.repeat(3)); // "Hello Hello Hello "
```
**str.length (immutable)**: Returns the length of a string.
```
let str = "Hello, World!";
console.log(str.length); // 13
```
**str.trimStart() (immutable)**: Returns a new string with leading whitespace removed.

```
let str = "   Hello, World!";
console.log(str.trimStart()); // "Hello, World!"
```
**str.trimEnd() (immutable)**: Returns a new string with trailing whitespace removed.
```
let str = "Hello, World!   ";
console.log(str.trimEnd()); // "Hello, World!"
```

### OBJECT METHODS

**Mutable Object Methods/Operations:**

Assigning a new property or modifying an existing property:

```
let person = {
  name: "John",
  age: 30
};

// Adding a new property
person.city = "New York"; // { name: 'John', age: 30, city: 'New York' }

// Modifying an existing property
person.age = 31; // { name: 'John', age: 31, city: 'New York' }
```
**Object.assign() (mutable):** Copies the values of all enumerable own properties from one or more source objects to a target object.
```
let person = {
  name: "John",
  age: 30
};

let newPerson = Object.assign({}, person, { city: "New York" });
// newPerson = { name: 'John', age: 30, city: 'New York' }
// person is not modified
```

**Immutable Object Methods/Operations:**

**Object.entries() (immutable):** Returns an array of a given object's own enumerable string-keyed property [key, value] pairs.

```
let person = {
  name: "John",
  age: 30
};

let entries = Object.entries(person); // [['name', 'John'], ['age', 30]]
```

**Object.keys() (immutable)**: Returns an array of a given object's own enumerable property names.
```
let person = {
  name: "John",
  age: 30
};

let keys = Object.keys(person); // ['name', 'age']
```
**Object.values() (immutable):** Returns an array of a given object's own enumerable property values.
```
let person = {
  name: "John",
  age: 30
};
let values = Object.values(person); // ['John', 30]
```

**Object.freeze() (immutable):** Freezes an object, preventing new properties from being added, existing properties from being removed, and existing properties or their enumerability, configurability, or writability from being changed.

```
let person = {
  name: "John",
  age: 30
};
let frozenPerson = Object.freeze(person);
frozenPerson.name = "Jane"; // Fails silently in non-strict mode
frozenPerson.city = "New York"; // Fails silently in non-strict mode

```
### Hoisting:
**Hoisting** is the behavior in JavaScript where variable and function declarations are moved to the top of their respective scopes (global or local) during the compilation phase, before the code is executed.
```
//Example:
console.log(x); // Output: undefined
var x = 5;

function hello() {
  console.log("Hello!");
}

hello(); // Output: "Hello!"
```


### Scopes:
**Scope** in JavaScript refers to the accessibility or visibility of variables and functions within a certain part of the code. JavaScript has three types of scopes: global scope, function scope (or local scope), and block scope (introduced in ES6 with let and const).
```
//Example:
// Global scope
let globalVar = "Hello";

function myFunction() {
  // Function scope
  let localVar = "World";
  console.log(globalVar); // Accessible: "Hello"
  console.log(localVar); // Accessible: "World"

  if (true) {
    // Block scope
    const blockVar = "Block";
    console.log(blockVar); // Accessible: "Block"
  }

  console.log(blockVar); // Not accessible, causes an error
}

myFunction();
console.log(localVar); // Not accessible, causes an error
```

### Closures:
**closure** is a function that has access to variables from an outer (enclosing) function, even after the outer function has finished executing. Closures allow you to create private variables and methods, and they are commonly used in JavaScript for data privacy, event handling, and creating function factories.
```
//Example:
function outerFunction() {
  let outerVar = "I am outside";

  function innerFunction() {
    let innerVar = "I am inside";
    console.log(outerVar); // Accessible: "I am outside"
    console.log(innerVar); // Accessible: "I am inside"
  }

  return innerFunction;
}

let closure = outerFunction();
closure(); // Outputs: "I am outside" and "I am inside"
```
### Higher-order function 
**Higher-order functions** are functions that can take other functions as arguments or return functions as their result. 

```
function applyOperation(operation, a, b) {
  return operation(a, b);
}

function add(x, y) {
  return x + y;
}

function multiply(x, y) {
  return x * y;
}

console.log(applyOperation(add, 2, 3)); // Output: 5
console.log(applyOperation(multiply, 4, 5)); // Output: 20
```

### Best Practices

**Follow a Consistent Coding Style:**
```
// Camel case for variables and functions
const myVariable = 'value';
function myFunction() { /* ... */ }

// Use descriptive names
const userProfile = { /* ... */ };
function calculateTotal() { /* ... */ }
```

**Use Semicolons:**
```
const x = 1;
const y = 2;

console.log(x + y); // 3
```

**Declare Variables with const or let:**
```
const PI = 3.14159; // Use const for constants
let radius = 5; // Use let for variables that may change
```
Avoid using **var** to declare variables, as it can lead to unexpected behavior with hoisting and scoping.

```
//bad practice
for(let i=0;i<n;i++){
  //code
}

//good practice

for(let index = 0 ;index < n;index ++ ){
  //code
}

```

### Debugger
- The debugger statement invokes any available debugging functionality, such as setting a breakpoint. If no debugging functionality is available, this statement has no effect.

```
function potentiallyBuggyCode() {
  debugger;
  // do potentially buggy stuff to examine, step through, etc.
}
```

